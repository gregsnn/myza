﻿using AutoMapper;
using FilmesApi.Data;
using FilmesApi.Data.Dtos;
using FilmesApi.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace FilmesApi.Services
{
    public class FilmeCrudService : ControllerBase
    {

        private FilmeContext _context;
        private IMapper _mapper;

        public FilmeCrudService(FilmeContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public Filme Create(CreateFilmeDto dto)
        {
            Filme filme = _mapper.Map<Filme>(dto);
            _context.Filmes.Add(filme);
            _context.SaveChanges();

            return filme;
        }

        public IEnumerable<ReadFilmeDto> Get(int skip, int take, string? nomeCinema)
        {
            if (nomeCinema == null)
            {
                return _mapper.Map<List<ReadFilmeDto>>(_context.Filmes.Skip(skip).Take(take).ToList());
            }

            return _mapper.Map<List<ReadFilmeDto>>(
                _context.Filmes
                .Skip(skip)
                .Take(take)
                .Where(filme => filme.Sessoes
                    .Any(sessao => sessao.Cinema.Nome.Contains(nomeCinema))).ToList());
        }

        public ReadFilmeDto GetById(int id)
        {
            Filme filme = _context.Filmes.FirstOrDefault(filme => filme.Id == id);

            if (filme == null)
            {
                throw new Exception("Filme não encontrado");
            }

            return _mapper.Map<ReadFilmeDto>(filme);
        }

        public void Update(int id, UpdateFilmeDto dto)
        {
            Filme filme = _context.Filmes.FirstOrDefault(filme => filme.Id == id);

            if (filme == null)
            {
                throw new Exception("Filme não encontrado");
            }

            _mapper.Map(dto, filme);
            _context.SaveChanges();
        }

        public void Patch(int id, JsonPatchDocument<UpdateFilmeDto> patch)
        {
            Filme filme = _context.Filmes.FirstOrDefault(filme => filme.Id == id);

            if (filme == null)
            {
                throw new Exception("Filme não encontrado");
            }

            var filmeParaAtualizar = _mapper.Map<UpdateFilmeDto>(filme);

            patch.ApplyTo(filmeParaAtualizar, ModelState);

            if (!TryValidateModel(filmeParaAtualizar))
            {
                var error = ValidationProblem(ModelState);
                throw new Exception(error.ToString());
            }

            _mapper.Map(filmeParaAtualizar, filme);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            Filme filme = _context.Filmes.FirstOrDefault(filme => filme.Id == id);

            if (filme == null)
            {
                throw new Exception("Filme não encontrado");
            }

            _context.Remove(filme);
            _context.SaveChanges();
        }
    }
}
