﻿using AutoMapper;
using FilmesApi.Data;
using FilmesApi.Data.Dtos;
using FilmesApi.Models;

namespace FilmesApi.Services
{
    public class SessaoCrudService
    {
        private FilmeContext _context;
        private IMapper _mapper;

        public SessaoCrudService(IMapper mapper, FilmeContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        public Sessao Create(CreateSessaoDto dto)
        {
            Sessao sessao = _mapper.Map<Sessao>(dto);
            _context.Sessoes.Add(sessao);
            _context.SaveChanges();

            return sessao;
        }

        public IEnumerable<ReadSessaoDto> Get()
        {
            return _mapper.Map<List<ReadSessaoDto>>(_context.Sessoes.ToList());
        }

        public ReadSessaoDto GetById(int filmeId, int cinemaId)
        {
            Sessao sessao = _context.Sessoes.FirstOrDefault(sessao => sessao.FilmeId == filmeId && sessao.CinemaId == cinemaId);

            if (sessao == null)
            {
                throw new Exception("Sessão não encontrada");
            }

            return _mapper.Map<ReadSessaoDto>(sessao);
        }
    }
}
