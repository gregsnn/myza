﻿using AutoMapper;
using FilmesApi.Data;
using FilmesApi.Data.Dtos;
using FilmesApi.Models;
using Microsoft.EntityFrameworkCore;

namespace FilmesApi.Services
{
    public class CinemaCrudService
    {
        private FilmeContext _context;
        private IMapper _mapper;

        public CinemaCrudService(IMapper mapper, FilmeContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        public Cinema Create(CreateCinemaDto dto)
        {
            Cinema cinema = _mapper.Map<Cinema>(dto);
            _context.Cinemas.Add(cinema);
            _context.SaveChanges();

            return cinema;
        }

        public IEnumerable<ReadCinemaDto> Get(int? enderecoId)
        {
            if (enderecoId == null)
            {
                return _mapper.Map<List<ReadCinemaDto>>(_context.Cinemas.ToList());
            }

            return _mapper.Map<List<ReadCinemaDto>>(
                _context.Cinemas
                        .FromSqlRaw($"SELECT Id, Nome, EnderecoId FROM cinemas WHERE cinemas.EnderecoId = {enderecoId}").ToList()
            );
        }

        public ReadCinemaDto GetById(int id)
        {
            Cinema cinema = _context.Cinemas.FirstOrDefault(cinema => cinema.Id == id);

            if (cinema == null)
            {
                throw new Exception("Cinema não encontrado");
            }

            return _mapper.Map<ReadCinemaDto>(cinema);
        }

        public void Update(int id, UpdateCinemaDto dto)
        {
            Cinema cinema = _context.Cinemas.FirstOrDefault(cinema => cinema.Id == id);

            if (cinema == null)
            {
                throw new Exception("Cinema não encontrado");
            }

            _mapper.Map(dto, cinema);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            Cinema cinema = _context.Cinemas.FirstOrDefault(cinema => cinema.Id == id);

            if (cinema == null)
            {
                throw new Exception("Cinema não encontrado");
            }

            _context.Remove(cinema);
            _context.SaveChanges();
        }
    }
}
