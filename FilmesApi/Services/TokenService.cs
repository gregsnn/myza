﻿using FilmesApi.Models;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace FilmesApi.Services
{
    public class TokenService
    {
        public string GenerateToken(Usuario usuario)
        {
            Claim[] claims = new Claim[]
            {
                new Claim("username", usuario.UserName),
                new Claim("id", usuario.Id),
                new Claim(ClaimTypes.DateOfBirth, usuario.DataNascimento.ToString("yyyy-MM-dd")),
            };

            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("QWERTY12345QWERTY1234"));

            SigningCredentials signinCredential = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                            issuer: "MyzaAPI",
                            audience: "Public",
                            claims: claims,
                            expires: DateTime.Now.AddMinutes(30),
                            signingCredentials: signinCredential
                        );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}