﻿using AutoMapper;
using FilmesApi.Data;
using FilmesApi.Data.Dtos;
using FilmesApi.Models;

namespace FilmesApi.Services
{
    public class EnderecoCrudService
    {
        private FilmeContext _context;
        private IMapper _mapper;

        public EnderecoCrudService(IMapper mapper, FilmeContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        public Endereco Create(CreateEnderecoDto dto)
        {
            Endereco endereco = _mapper.Map<Endereco>(dto);
            _context.Enderecos.Add(endereco);
            _context.SaveChanges();

            return endereco;
        }

        public IEnumerable<ReadEnderecoDto> Get()
        {
            return _mapper.Map<List<ReadEnderecoDto>>(_context.Enderecos);
        }

        public ReadEnderecoDto GetById(int id)
        {
            Endereco endereco = _context.Enderecos.FirstOrDefault(endereco => endereco.Id == id);

            if (endereco == null)
            {
                throw new Exception("Endereço não encontrado");
            }

            return _mapper.Map<ReadEnderecoDto>(endereco);
        }

        public void Update(int id, UpdateEnderecoDto dto)
        {
            Endereco endereco = _context.Enderecos.FirstOrDefault(endereco => endereco.Id == id);

            if (endereco == null)
            {
                throw new Exception("Endereço não encontrado");
            }

            _mapper.Map(dto, endereco);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            Endereco endereco = _context.Enderecos.FirstOrDefault(endereco => endereco.Id == id);

            if (endereco == null)
            {
                throw new Exception("Endereço não encontrado");
            }

            _context.Remove(endereco);
            _context.SaveChanges();
        }
    }
}
