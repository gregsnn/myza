﻿using FilmesApi.Data.Dtos;
using FilmesApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace FilmesApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsuarioController : ControllerBase
    {
        private UsuarioCrudService _usuarioCrudService;
        private UsuarioLoginService _usuarioLoginService;

        public UsuarioController(UsuarioCrudService cadastroService, UsuarioLoginService usuarioLoginService)
        {
            _usuarioCrudService = cadastroService;
            _usuarioLoginService = usuarioLoginService;
        }

        /// <summary>
        /// Adiciona um Usuario ao banco de dados
        /// </summary>
        /// <param name="usuarioDto">Objeto com os campos necessários para criação de um Usuario</param>
        /// <returns>IActionResult</returns>
        /// <response code="201">Caso inserção seja feita com sucesso</response>
        [HttpPost("cadastro")]
        public async Task<IActionResult> CadastraUsuario(CreateUsuarioDto createUsuarioDto)
        {
            try
            {
                await _usuarioCrudService.Cadastra(createUsuarioDto);
                return CreatedAtAction(nameof(CadastraUsuario), createUsuarioDto);
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new { Erro = ex.Message });
            }
        }

        /// <summary>
        /// Realiza o Login do Usuario
        /// </summary>
        /// <param name="loginUsuarioDto">Objeto com os campos necessários para o Login do Usuario</param>
        /// <returns>IActionResult</returns>
        /// <response code="201">Caso o login seja feita com sucesso</response>
        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginUsuarioDto loginUsuarioDto)
        {
            try
            {
                string token = await _usuarioLoginService.Login(loginUsuarioDto);
                return Ok(token);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
