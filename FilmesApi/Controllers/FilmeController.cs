﻿using FilmesApi.Data.Dtos;
using FilmesApi.Models;
using FilmesApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace FilmesApi.Controllers;

[ApiController]
[Route("[controller]")]
public class FilmeController : ControllerBase, IFilme
{
    private FilmeCrudService _filmeCrudService;

    public FilmeController(FilmeCrudService filmeService)
    {
        _filmeCrudService = filmeService;
    }

    /// <summary>
    /// Adiciona um filme ao banco de dados
    /// </summary>
    /// <param name="filmeDto">Objeto com os camnpos necessários para criação de um filme</param>
    /// <returns>IActionResult</returns>
    /// <response code="201">Caso inserção seja feita com sucesso</response>
    [HttpPost]
    public IActionResult AdicionaFilme([FromBody] CreateFilmeDto filmeDto)
    {
        Filme filme = _filmeCrudService.Create(filmeDto);
        return CreatedAtAction(nameof(RecuperaFilmePorId), new { id = filme.Id }, filme);
    }

    /// <summary>
    /// Retorna filmes paginados
    /// </summary>
    /// <param name="skip">filmes para pular</param>
    /// <param name="take">range de filmes</param>
    /// <param name="nomeCinema"></param>
    /// <returns>IEnumerable</returns>
    [HttpGet]
    public IEnumerable<ReadFilmeDto> RecuperaFilmes([FromQuery] int skip = 0, [FromQuery] int take = 50, [FromQuery] string? nomeCinema = null)
    {
        return _filmeCrudService.Get(skip, take, nomeCinema);
    }

    /// <summary>
    /// Retorna um filme por id
    /// </summary>
    /// <param name="id">Id do filme</param>
    /// <returns>IActionResult</returns>
    /// <response code="200">Caso o filme seja encontrado</response>
    /// <response code="404">Caso o filme não seja encontrado</response>
    [HttpGet("{id}")]
    public IActionResult RecuperaFilmePorId(int id)
    {
        try
        {
            ReadFilmeDto filme = _filmeCrudService.GetById(id);
            return Ok(filme);
        }
        catch (Exception ex)
        {
            return NotFound(ex);
        }
    }

    /// <summary>
    /// Atualiza um filme por id e um objeto de atualização
    /// </summary>
    /// <param name="id">Id do filme</param>
    /// <param name="filmeDto">Filme atualizado</param>
    /// <returns>IActionResult</returns>
    /// <response code="204">Caso o filme seja atualizado com sucesso</response>
    /// <response code="404">Caso o filme não seja encontrado</response>
    [HttpPut("{id}")]
    public IActionResult AtualizaFilme(int id, [FromBody] UpdateFilmeDto filmeDto)
    {
        try
        {
            _filmeCrudService.Update(id, filmeDto);
            return NoContent();
        }
        catch (Exception ex)
        {
            return NotFound(ex);
        }
    }

    //[HttpPatch("{id}")]
    //public IActionResult AtualizaFilmeParcial(int id, JsonPatchDocument<UpdateFilmeDto> patch)
    //{
    //    try
    //    {
    //        _filmeService.Patch(id, patch);
    //        return NoContent();
    //    }
    //    catch (Exception ex)
    //    {
    //        return NotFound(ex);
    //    }
    //}


    /// <summary>
    /// Deleta um filme por id
    /// </summary>
    /// <param name="id">Id do filme</param>
    /// <returns>IActionResult</returns>
    /// <response code="204">Caso o filme seja deletado com sucesso</response>
    /// <response code="404">Caso o filme não seja encontrado</response>
    [HttpDelete("{id}")]
    public IActionResult DeletaFilme(int id)
    {
        try
        {
            _filmeCrudService.Delete(id);
            return NoContent();
        }
        catch (Exception ex)
        {
            return NotFound(ex);
        }
    }
}
