﻿using FilmesApi.Data.Dtos;
using FilmesApi.Models;
using FilmesApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace FilmesApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SessaoController : ControllerBase
    {
        private SessaoCrudService _sessaoCrudService;

        public SessaoController(SessaoCrudService sessaoService)
        {
            _sessaoCrudService = sessaoService;
        }

        /// <summary>
        /// Adiciona um Sessao ao banco de dados
        /// </summary>
        /// <param name="sessaoDto">Objeto com os campos necessários para criação de um Sessao</param>
        /// <returns>IActionResult</returns>
        /// <response code="201">Caso inserção seja feita com sucesso</response>
        [HttpPost]
        public IActionResult AdicionaSessao(CreateSessaoDto sessaoDto)
        {
            Sessao sessao = _sessaoCrudService.Create(sessaoDto);
            return CreatedAtAction(nameof(RecuperaSessoesPorId), new { filmeId = sessao.FilmeId, cinemaId = sessao.CinemaId }, sessao);
        }

        /// <summary>
        /// Retorna sessao paginados
        /// </summary>
        /// <returns>IEnumerable</returns>
        [HttpGet]
        public IEnumerable<ReadSessaoDto> RecuperaSessoes()
        {
            return _sessaoCrudService.Get();
        }

        /// <summary>
        /// Retorna um sessao por id
        /// </summary>
        /// <param name="filmeId">Id do filme</param>
        /// <param name="cinemaId">Id do cinema</param>
        /// <returns>IActionResult</returns>
        /// <response code="200">Caso o sessao seja encontrado</response>
        /// <response code="404">Caso o sessao não seja encontrado</response>
        [HttpGet("{filmeId}/{cinemaId}")]
        public IActionResult RecuperaSessoesPorId(int filmeId, int cinemaId)
        {
            try
            {
                ReadSessaoDto sessao = _sessaoCrudService.GetById(filmeId, cinemaId);
                return Ok(sessao);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

    }
}