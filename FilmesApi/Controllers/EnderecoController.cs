﻿using FilmesApi.Data.Dtos;
using FilmesApi.Models;
using FilmesApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace FilmesApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EnderecoController : ControllerBase
    {
        private EnderecoCrudService _enderecoCrudService;

        public EnderecoController(EnderecoCrudService enderecoService)
        {
            _enderecoCrudService = enderecoService;
        }

        /// <summary>
        /// Adiciona um Endereço ao banco de dados
        /// </summary>
        /// <param name="enderecoDto">Objeto com os campos necessários para criação de um Endereco</param>
        /// <returns>IActionResult</returns>
        /// <response code="201">Caso inserção seja feita com sucesso</response>
        [HttpPost]
        public IActionResult AdicionaEndereco([FromBody] CreateEnderecoDto enderecoDto)
        {
            Endereco endereco = _enderecoCrudService.Create(enderecoDto);
            return CreatedAtAction(nameof(RecuperaEnderecosPorId), new { id = endereco.Id }, endereco);
        }

        /// <summary>
        /// Retorna endereço paginados
        /// </summary>
        /// <returns>IEnumerable</returns>
        [HttpGet]
        public IEnumerable<ReadEnderecoDto> RecuperaEnderecos()
        {
            return _enderecoCrudService.Get();
        }

        /// <summary>
        /// Retorna um endereço por id
        /// </summary>
        /// <param name="id">Id do endereço</param>
        /// <returns>IActionResult</returns>
        /// <response code="200">Caso o endereço seja encontrado</response>
        /// <response code="404">Caso o endereço não seja encontrado</response>
        [HttpGet("{id}")]
        public IActionResult RecuperaEnderecosPorId(int id)
        {
            try
            {
                ReadEnderecoDto endereco = _enderecoCrudService.GetById(id);
                return Ok(endereco);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        /// <summary>
        /// Atualiza um endereço por id e um objeto de atualização
        /// </summary>
        /// <param name="id">Id do endereço</param>
        /// <param name="enderecoDto">Endereço atualizado</param>
        /// <returns>IActionResult</returns>
        /// <response code="204">Caso o endereço seja atualizado com sucesso</response>
        /// <response code="404">Caso o endereço não seja encontrado</response>
        [HttpPut("{id}")]
        public IActionResult AtualizaEndereco(int id, [FromBody] UpdateEnderecoDto enderecoDto)
        {
            try
            {
                _enderecoCrudService.Update(id, enderecoDto);
                return NoContent();
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        /// <summary>
        /// Deleta um endereço por id
        /// </summary>
        /// <param name="id">Id do endereço</param>
        /// <returns>IActionResult</returns>
        /// <response code="204">Caso o endereço seja deletado com sucesso</response>
        /// <response code="404">Caso o endereço não seja encontrado</response>
        [HttpDelete("{id}")]
        public IActionResult DeletaEndereco(int id)
        {
            try
            {
                _enderecoCrudService.Delete(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }
    }
}
