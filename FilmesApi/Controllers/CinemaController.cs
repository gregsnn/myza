﻿using FilmesApi.Data.Dtos;
using FilmesApi.Models;
using FilmesApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace FilmesApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CinemaController : ControllerBase
    {

        private CinemaCrudService _cinemaCrudService;

        public CinemaController(CinemaCrudService cinemaService)
        {
            _cinemaCrudService = cinemaService;
        }

        /// <summary>
        /// Adiciona um Cinema ao banco de dados
        /// </summary>
        /// <param name="cinemaDto">Objeto com os campos necessários para criação de um Cinema</param>
        /// <returns>IActionResult</returns>
        /// <response code="201">Caso inserção seja feita com sucesso</response>
        [HttpPost]
        public IActionResult AdicionaCinema([FromBody] CreateCinemaDto cinemaDto)
        {

            Cinema cinema = _cinemaCrudService.Create(cinemaDto);
            return CreatedAtAction(nameof(RecuperaCinemasPorId), new { id = cinema.Id }, cinema);
        }

        /// <summary>
        /// Retorna cinemas paginados
        /// </summary>
        /// <param name="enderecoId">Id do endereço</param>
        /// <returns>IEnumerable</returns>
        [HttpGet]
        public IEnumerable<ReadCinemaDto> RecuperaCinemas([FromQuery] int? enderecoId = null)
        {

            return _cinemaCrudService.Get(enderecoId);
        }

        /// <summary>
        /// Retorna um cinema por id
        /// </summary>
        /// <param name="id">Id do cinema</param>
        /// <returns>IActionResult</returns>
        /// <response code="200">Caso o cinema seja encontrado</response>
        /// <response code="404">Caso o cinema não seja encontrado</response>
        [HttpGet("{id}")]
        public IActionResult RecuperaCinemasPorId(int id)
        {
            try
            {
                ReadCinemaDto cinema = _cinemaCrudService.GetById(id);
                return Ok(cinema);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        /// <summary>
        /// Atualiza um cinema por id e um objeto de atualização
        /// </summary>
        /// <param name="id">Id do cinema</param>
        /// <param name="cinemaDto">Cinema atualizado</param>
        /// <returns>IActionResult</returns>
        /// <response code="204">Caso o cinema seja atualizado com sucesso</response>
        /// <response code="404">Caso o cinema não seja encontrado</response>
        [HttpPut("{id}")]
        public IActionResult AtualizaCinema(int id, [FromBody] UpdateCinemaDto cinemaDto)
        {
            try
            {
                _cinemaCrudService.Update(id, cinemaDto);
                return NoContent();
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        /// <summary>
        /// Deleta um cinema por id
        /// </summary>
        /// <param name="id">Id do cinema</param>
        /// <returns>IActionResult</returns>
        /// <response code="204">Caso o cinema seja deletado com sucesso</response>
        /// <response code="404">Caso o cinema não seja encontrado</response>
        [HttpDelete("{id}")]
        public IActionResult DeletaCinema(int id)
        {
            try
            {
                _cinemaCrudService.Delete(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

    }
}
